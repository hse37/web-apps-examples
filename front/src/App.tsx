import React from 'react'
import './App.css'
import {createBrowserRouter, Link, RouterProvider} from "react-router-dom";
import "./routes";
import RootPage from "./routes/root";
import TodoListPage from "./routes/todo-list";
import AboutPage from "./routes/about";
import IndexPage from "./routes/index";


const router = createBrowserRouter([
    {
        path: "/",
        element: <RootPage/>,
        children: [
            {
                index: true,
                element: <IndexPage />
            },
            {
                path: "/todo-list",
                element: <TodoListPage/>,
            },
            {
                path: "/about",
                element: <AboutPage/>
            }
        ],
    },

]);

function App() {
    return (
        <div className="App">
            <p className="red-centered-text">Пример HTML страницы</p>

            <RouterProvider router={router}/>
        </div>
    )
}

export default App
