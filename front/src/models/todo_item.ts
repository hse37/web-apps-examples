interface TodoItem {
    id: number,
    title: string,
}

export default TodoItem
