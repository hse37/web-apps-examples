import asyncio

from fastapi import FastAPI, Request
from fastapi.responses import HTMLResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates

app = FastAPI()

app.mount("/static", StaticFiles(directory="static"), name="static")

templates = Jinja2Templates(directory="templates")


@app.get("/todo-list", response_class=HTMLResponse)
async def get_todo_list(request: Request):
    # предположим данные получены из базы данных
    todo_list = [
        {'id': 1, 'title': 'Завтрак'},
        {'id': 2, 'title': 'Лекции в любимом универе'},
        {'id': 3, 'title': 'Мультики'},
        {'id': 4, 'title': 'Поспать'},
    ]

    return templates.TemplateResponse(
        "todo-list.html",
        {
            'request': request,
            'todo_list': todo_list,
        }
    )


@app.get("/about", response_class=HTMLResponse)
async def about(request: Request):
    return templates.TemplateResponse("about.html", {'request': request})


@app.get("/", response_class=HTMLResponse)
async def index(request: Request):
    return templates.TemplateResponse("index.html", {'request': request})



@app.get("/api/todo-list")
async def get_todo_list_api():
    todo_list = [
        {'id': 1, 'title': 'Завтрак'},
        {'id': 2, 'title': 'Лекции в любимом универе'},
        {'id': 3, 'title': 'Мультики'},
        {'id': 4, 'title': 'Поспать'},
    ]

    return todo_list
