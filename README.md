
Запуск backend (FastAPI)

```shell
uvicorn main:app --host 0.0.0.0 --port 8000
```

Запуск SPA в режиме разработки

```shell
cd front
npm run dev
```
